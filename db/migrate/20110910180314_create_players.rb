class CreatePlayers < ActiveRecord::Migration
  def self.up
    create_table :players do |t|
      t.string :name
      t.integer :team_id
      t.text :description
      t.string :url
      t.stringd :twitter

      t.timestamps
    end
  end

  def self.down
    drop_table :players
  end
end
